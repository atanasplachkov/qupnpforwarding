#include <QCoreApplication>
#include "QUPnPForwarding/qupnpforwarding.h"

void onError(QString msg) {
	qDebug() << msg;
}

void onSuccess(QString description) {
	qDebug() << description;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	QUPnPForwarding upnp;
	upnp.addMappingQueue("Test UPnP", "192.168.0.100", 5000, 5000, "TCP");
	upnp.start();

	a.connect(&upnp, &QUPnPForwarding::error, onError);
	a.connect(&upnp, &QUPnPForwarding::success, onSuccess);

	return a.exec();
}
