#include "qupnpforwarding.h"

QUPnPForwarding::QUPnPForwarding(QObject *parent) :
	QObject(parent),
	discoveryUdp(NULL),
	xmlGetManager(NULL),
	soapPostManager(NULL)
{
}

QUPnPForwarding::~QUPnPForwarding()
{
	cleanUp();
}

QString QUPnPForwarding::getLastError()
{
	return lastError;
}

void QUPnPForwarding::addMappingQueue(QString description, QString ip, quint16 internalPort, quint16 externalPort, QString protocol)
{
	Queue q;
	q.command = ADD_PORT_MAPPING;
	q.description = description;
	q.ip = ip;
	q.internalPort = internalPort;
	q.externalPort = externalPort;
	q.protocol = protocol;

	queue.append(q);
}

void QUPnPForwarding::deleteMappingQueue(quint16 externalPort, QString protocol)
{
	Queue q;
	q.command = DEL_PORT_MAPPING;
	q.externalPort = externalPort;
	q.protocol = protocol;

	queue.append(q);
}

void QUPnPForwarding::start()
{
	cachedReplies.clear();
	cachedLocations.clear();
	cachedLocationsReplies.clear();

	discovery();
}

void QUPnPForwarding::discovery()
{
	if (discoveryUdp) {
		delete discoveryUdp;
	}

	QHostAddress host(DISCOVERY_MULTICAST_ADDRESS);
	quint16 port = DISCOVERY_MULTICAST_PORT;

	discoveryUdp = new QUdpSocket(this);

	connect(discoveryUdp, SIGNAL(readyRead()), this, SLOT(discoveryResponse()));
	discoveryUdp->writeDatagram(
		QString(MSEARCH).arg(GATEWAY_DEVICE).arg(3).toUtf8(),
		QHostAddress::Broadcast,
		port
	);
}

void QUPnPForwarding::controlXmlRequest(QString &location)
{
	if (location.isEmpty()) {
		lastError = "LOCATION header is unknown.";
		emit error(lastError);
		return;
	}

	if (!xmlGetManager) {
		xmlGetManager = new QNetworkAccessManager(this);
		connect(xmlGetManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(controlXmlResponse(QNetworkReply *)));
	}

	QNetworkReply *resp = xmlGetManager->get(QNetworkRequest(QUrl(location)));
	cachedLocationsReplies[resp] = location;
}

void QUPnPForwarding::controlSoapRequest(QString &baseURL, QString &controlURL, QString &serviceType)
{
	if (controlURL.isEmpty()) {
		lastError = "UPnP controlURL is unknown.";
		emit error(lastError);
		return;
	}

	if (!soapPostManager) {
		soapPostManager = new QNetworkAccessManager(this);
		connect(soapPostManager, SIGNAL(finished(QNetworkReply*)), SLOT(controlSoapResponse(QNetworkReply*)));
	}

	QUrl url = QUrl(baseURL + controlURL);
	QNetworkRequest request(url);
	request.setRawHeader("Content-Type", "text/xml");

	for (int i = 0, len = queue.length(); i < len; i++) {
		Queue q = queue.first();
		QString body;

		if (q.command == ADD_PORT_MAPPING) {
			body = QString(SOAP_ADD_MAPPING_BODY)
					.arg(q.externalPort)
					.arg(q.protocol)
					.arg(q.internalPort)
					.arg(q.ip)
					.arg(q.description)
					.arg(DEFAULT_LEASE_DURATION);
		} else if (q.command == DEL_PORT_MAPPING) {
			body = QString(SOAP_DEL_MAPPING_BODY)
					.arg(q.externalPort)
					.arg(q.protocol);
		} else {
			queue.removeFirst();
			continue;
		}

		QByteArray data = QString(SOAP_PORT_MAPPING_ENVELOPE)
				.arg(q.command)
				.arg(serviceType)
				.arg(body)
				.arg(q.command)
				.toUtf8();

		QByteArray size = QByteArray::number(data.size());

		request.setRawHeader("SOAPAction", QString("\"%1#%2\"").arg(serviceType).arg(q.command).toUtf8());
		request.setRawHeader("Content-Length", size);

		QNetworkReply *resp = soapPostManager->post(request, data);
		cachedReplies[resp] = q;
		queue.removeFirst();
	}
}

void QUPnPForwarding::discoveryResponse()
{
	QByteArray data;
	char buffer[1024];

	while (discoveryUdp->hasPendingDatagrams()) {
		int sz = discoveryUdp->readDatagram(buffer, 1024);

		if (sz > -1) {
			data.append(buffer, sz);
		}
	}

	do {
		int end = data.indexOf("\r\n\r\n");
		QByteArray partial;

		if (end > -1) {
			partial = data.left(end);
			data = data.mid(end + 4);
		} else {
			partial = data;
		}

		QString location;
		QList<QByteArray> lines = partial.split('\n');
		foreach (QByteArray line, lines) {
			line = line.trimmed();

			int pos = line.indexOf(':');
			if (pos > -1) {
				QByteArray header = line.left(pos).trimmed().toLower();
				QByteArray value = line.mid(pos + 1).trimmed();

				if (header == "location") {
					location = value;
				}
			}
		}

		if (!cachedLocations.contains(location)) {
			cachedLocations.append(location);
			controlXmlRequest(location);
		}
	} while (data.length() > 0);
}

void QUPnPForwarding::controlXmlResponse(QNetworkReply *response)
{
	QDomDocument dom;
	QString baseURL;
	QString controlURL;
	QString eventURL;
	QString serviceType;
	QString errorMsg;

	QByteArray data = response->readAll();
	bool res = dom.setContent(data, &errorMsg);

	if (!res) {
		lastError = "Could not parse the control XML. " + errorMsg;
		emit error(lastError);
		return;
	}

	QDomElement root = dom.documentElement();

	QDomElement urlBase = root.firstChildElement("URLBase");
	if (!urlBase.isNull()) {
		baseURL = urlBase.text();
	}

	if (baseURL.isEmpty()) {
		QUrl url(cachedLocationsReplies[response]);
		baseURL = url.scheme() + "://" + url.host() + ":" + QString("%1").arg(url.port());
		cachedLocationsReplies.remove(response);
	}

	QDomElement node = findWANService(root);
	if (!node.isNull()) {
		serviceType = node.firstChildElement("serviceType").text();
		controlURL = node.firstChildElement("controlURL").text();
		eventURL = node.firstChildElement("eventSubURL").text();

		QDomElement URLBase = node.firstChildElement("URLBase");
		if (!URLBase.isNull()) {
			baseURL = URLBase.text();
		}
	}

	response->deleteLater();
	controlSoapRequest(baseURL, controlURL, serviceType);
}

void QUPnPForwarding::controlSoapResponse(QNetworkReply *response)
{
	int code = response->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	Queue q = cachedReplies[response];

	if (code == 200) {
		emit success(q.description);
	} else {
		emit error(
					QString("Response code: %1\nDetail: %2")
						.arg(code)
						.arg(QString(response->readAll())));
	}

	cachedReplies.remove(response);

	response->deleteLater();
}

QDomElement QUPnPForwarding::findWANService(QDomElement node)
{
	QDomElement device = node.firstChildElement("device");
	if (device.isNull()) {
		return device;
	}

	QDomElement deviceList = device.firstChildElement("deviceList");
	if (!deviceList.isNull()) {
		QDomElement node = findWANService(deviceList);
		if (!node.isNull()) {
			return node;
		}
	}

	QDomElement serviceList = device.firstChildElement("serviceList");
	if (serviceList.isNull()) {
		return serviceList;
	}

	QDomElement service = serviceList.firstChildElement("service");
	while (!service.isNull()) {
		QDomElement serviceType = service.firstChildElement("serviceType");
		if (!serviceType.isNull()) {
			QString type = serviceType.text();
			if (type == SERVICE_WANIP || type == SERVICE_WANPPP) {
				return service;
			}
		}
		service = service.nextSiblingElement("service");
	}

	return QDomElement();
}

void QUPnPForwarding::cleanUp()
{
	if (discoveryUdp) delete discoveryUdp;
	if (xmlGetManager) delete xmlGetManager;
	if (soapPostManager) delete soapPostManager;
}
