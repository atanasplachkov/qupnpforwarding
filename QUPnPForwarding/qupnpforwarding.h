#ifndef QUPNPFORWARDING_H
#define QUPNPFORWARDING_H

#include <QObject>
#include <QMap>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDomDocument>
#include <QFile>

#define DISCOVERY_MULTICAST_ADDRESS "239.255.255.250"
#define DISCOVERY_MULTICAST_PORT 1900

#define MSEARCH	"M-SEARCH * HTTP/1.1\r\n" \
				"ST: %1\r\n" \
				"HOST: 239.255.255.250:1900\r\n" \
				"MAN: \"ssdp:discover\"\r\n" \
				"MX: %2\r\n" \
				"\r\n"

#define GATEWAY_DEVICE "urn:schemas-upnp-org:device:InternetGatewayDevice:1"

#define SERVICE_WANIP	"urn:schemas-upnp-org:service:WANIPConnection:1"
#define SERVICE_WANPPP	"urn:schemas-upnp-org:service:WANPPPConnection:1"

#define SOAP_PORT_MAPPING_ENVELOPE	"<?xml version=\"1.0\" encoding=\"utf-8\"?>" \
									"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" \
										"<s:Body>" \
											"<u:%1 xmlns:u=\"%2\">" \
												"%3" \
											"</u:%4>" \
										"</s:Body>" \
									"</s:Envelope>"

#define SOAP_ADD_MAPPING_BODY	"<NewRemoteHost></NewRemoteHost>" \
								"<NewExternalPort>%1</NewExternalPort>" \
								"<NewProtocol>%2</NewProtocol>" \
								"<NewInternalPort>%3</NewInternalPort>" \
								"<NewInternalClient>%4</NewInternalClient>" \
								"<NewEnabled>1</NewEnabled>" \
								"<NewPortMappingDescription>%5</NewPortMappingDescription>" \
								"<NewLeaseDuration>%6</NewLeaseDuration>"

#define SOAP_DEL_MAPPING_BODY	"<NewExternalPort>%1</NewExternalPort>" \
								"<NewProtocol>%2</NewProtocol>" \
								"<NewRemoteHost></NewRemoteHost>"


// One day
#define DEFAULT_LEASE_DURATION 60 * 60 * 24

#define ADD_PORT_MAPPING "AddPortMapping"
#define DEL_PORT_MAPPING "DeletePortMapping"

struct Queue {
	QString command;
	QString description;
	QString ip;
	quint16 internalPort;
	quint16 externalPort;
	QString protocol;
};

class QUPnPForwarding : public QObject
{
    Q_OBJECT
public:
    explicit QUPnPForwarding(QObject *parent = 0);
	~QUPnPForwarding();

	QString getLastError();

	void addMappingQueue(QString description, QString ip, quint16 internalPort, quint16 externalPort, QString protocol);
	void deleteMappingQueue(quint16 externalPort, QString protocol);
	void start();

signals:
	void success(QString description);
	void error(QString msg);

private:
	void discovery();
	void controlXmlRequest(QString &location);
	void controlSoapRequest(QString &baseURL, QString &controlURL, QString &serviceType);

	QUdpSocket *discoveryUdp;
	QNetworkAccessManager *xmlGetManager;
	QNetworkAccessManager *soapPostManager;
	QMap<QNetworkReply *, Queue> cachedReplies;
	QMap<QNetworkReply *, QString> cachedLocationsReplies;

	QList<QString> cachedLocations;
	QList<Queue> queue;

	QString lastError;

private slots:
	void discoveryResponse();
	void controlXmlResponse(QNetworkReply *response);
	void controlSoapResponse(QNetworkReply *response);

	QDomElement findWANService(QDomElement node);
	void cleanUp();
};

#endif // QUPNPFORWARDING_H
